import React from 'react'
import ReactDOM from 'react-dom/client'
import "./main.scss"
import {RouterProvider} from "react-router-dom";
import {router} from "./route.jsx";
import {Provider} from "react-redux";
import store from "./app/store.js";

ReactDOM.createRoot(document.getElementById('root')).render(
    <Provider store={store}>
        <RouterProvider router={router}/>
    </Provider>
)
