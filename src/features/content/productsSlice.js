import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

const initialState = {
    products: [],
    isLoading: false,
    error: null
}

export const productsFetch = createAsyncThunk(
    'products/productsFetch',
    async () => {
        const response = await fetch('products.json');
        const data = await response.json();
        return data;
    }
)

export const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(productsFetch.pending, (state) => {
                state.isLoading = true;
                state.error = null
            })
            .addCase(productsFetch.fulfilled, (state, action) => {
                state.isLoading = false;
                state.products = action.payload;
            })
            .addCase(productsFetch.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.error.message;
            })
    }
})

export default productsSlice;