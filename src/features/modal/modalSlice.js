import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    isModalOpen: false,
    modalType: null,
    modalData: {}
}

const modalReducer = createSlice({
    name: 'modal',
    initialState,
    reducers: {
        openModal: (state, actions) => {
            state.isModalOpen = true;
            state.modalType = actions.payload.modalType;
            state.modalData = actions.payload.modalData;
        },
        closeModal: (state) => {
            state.isModalOpen = false;
            state.modalType = null;
            state.modalData = {}
        }
    }
})


export const {openModal, closeModal} = modalReducer.actions
export default modalReducer;