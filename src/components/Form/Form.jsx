import React from 'react';
import {useFormik} from "formik";
import * as Yup from 'yup';
import "./Form.scss";

const validationSchema = Yup.object({
    firstName: Yup.string()
        .max(15, "Повинно бути не більше 15 літер")
        .required("Обов'язкове поле"),
    lastName: Yup.string()
        .min(4, "Прізвище повинно бути більше 4 літер")
        .required("Обов'язкове поле"),
    age: Yup.number()
        .required("Обов'язкове поле")
        .typeError("Тільки цифри"),
    address: Yup.string()
        .required("Обов'язкове поле"),
    tel: Yup.string()
        .matches(/^[0-9]{10}$/, "Невірний номер телефону")
        .required("Обов'язкове поле")
});

function Form({onSubmit}) {
    const formik = useFormik({
        initialValues: {
            firstName: "",
            lastName: "",
            age: "",
            address: "",
            tel: ""
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            onSubmit(values);
        }
    });

    return (
        <div className="basket-page__form-wrapper">
            <form className="basket-page__form" onSubmit={formik.handleSubmit}>
                <label className="basket-page__label">Ім'я</label>
                <input
                    className="basket-page__input-text"
                    type="text"
                    placeholder="Ім'я"
                    onChange={formik.handleChange}
                    value={formik.values.firstName}
                    name="firstName"
                    onBlur={formik.handleBlur}
                />
                {formik.touched.firstName && formik.errors.firstName ? (
                    <div className="form-errors">{formik.errors.firstName}</div>
                ) : null}

                <label className="basket-page__label">Прізвище</label>
                <input
                    className="basket-page__input-text"
                    type="text"
                    placeholder="Прізвище"
                    onChange={formik.handleChange}
                    value={formik.values.lastName}
                    name="lastName"
                    onBlur={formik.handleBlur}
                />
                {formik.touched.lastName && formik.errors.lastName ? (
                    <div className="form-errors">{formik.errors.lastName}</div>
                ) : null}

                <label className="basket-page__label">Вік</label>
                <input
                    className="basket-page__input-text"
                    type="text"
                    placeholder="Вік"
                    onChange={formik.handleChange}
                    value={formik.values.age}
                    name="age"
                    onBlur={formik.handleBlur}
                />
                {formik.touched.age && formik.errors.age ? (
                    <div className="form-errors">{formik.errors.age}</div>
                ) : null}

                <label className="basket-page__label">Адреса доставки</label>
                <input
                    className="basket-page__input-text"
                    type="text"
                    placeholder="Адреса доставки"
                    onChange={formik.handleChange}
                    value={formik.values.address}
                    name="address"
                    onBlur={formik.handleBlur}
                />
                {formik.touched.address && formik.errors.address ? (
                    <div className="form-errors">{formik.errors.address}</div>
                ) : null}

                <label className="basket-page__label">Телефон</label>
                <input
                    className="basket-page__input-text"
                    type="tel"
                    placeholder="Телефон"
                    onChange={formik.handleChange}
                    value={formik.values.tel}
                    name="tel"
                    onBlur={formik.handleBlur}
                />
                {formik.touched.tel && formik.errors.tel ? (
                    <div className="form-errors">{formik.errors.tel}</div>
                ) : null}

                <input className="basket-page__button" type="submit" value="Оформити замовлення"/>
            </form>
        </div>
    );
}

export default Form;
