import PropTypes from "prop-types";
import "./ProductList.scss"
import ProductCard from "../ProductCard/ProductCard.jsx";


function ProductsList({
                          items, onBuyClick, showStar = true,
                          showDelete = false, showButtonBuy = true,
                          isBasketPage = false
                      }) {

    return (
        // <div className="product-list__wrapper">
        //     {items.map((item, index) => (
        //         <ProductCard key={item.id}
        //                      item={item}
        //                      onBuyClick={onBuyClick}
        //                      showStar={showStar}
        //                      showDelete={showDelete}
        //                      showButtonBuy={showButtonBuy}
        //                      isBasketPage={isBasketPage}
        //         />
        //     ))}
        // </div>
        <>
            {items.map((item, index) => (
                <ProductCard key={item.id}
                             item={item}
                             onBuyClick={onBuyClick}
                             showStar={showStar}
                             showDelete={showDelete}
                             showButtonBuy={showButtonBuy}
                             isBasketPage={isBasketPage}
                />
            ))}
        </>
    );
}

ProductsList.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        imageUrl: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        article: PropTypes.string.isRequired
    })).isRequired,
    onBuyClick: PropTypes.func.isRequired,
    isBasketPage: PropTypes.bool
};


export default ProductsList;