import ModalImage from "../../components/ModalComponents/ModalImage/ModalImage.jsx";
import "../BasketPages/BasketPage.scss";
import { useDispatch, useSelector } from "react-redux";
import { closeModal, openModal } from "../../features/modal/modalSlice.js";
import { deleteToBasket, clearBasket } from "../../features/counter/counterSlice.js";
import ProductsList from "../../components/ProductsComponents/ProductList/ProductList.jsx";
import Form from "../../components/Form/Form.jsx";
import { useState } from "react";

function BasketPage() {
    const dispatch = useDispatch();
    const [isFormVisible, setFormVisible] = useState(true);
    const [orderPlaced, setOrderPlaced] = useState(false);

    const basket = useSelector((state) => state.counter.basket);
    const sumOrder = basket.reduce((sum, product) => sum + product.price, 0);
    const modal = useSelector((state) => state.modals);

    const confirmDeletionHandler = () => {
        dispatch(deleteToBasket(modal.modalData.id));
    };

    const handleOrderSubmit = (values) => {
        console.log("Дані клієнта", values, "Замовлені товари:", basket, "Cума замовлення:" , sumOrder);
        alert("Ваше замовлення оформлене!");
        dispatch(clearBasket());
        setFormVisible(false);
        setOrderPlaced(true);
    };

    return (
        <section className="basket-page">
            <div className="basket-page__wrapper-basket">
                {basket.length === 0 && !orderPlaced && (
                    <p className="basket-page__text-order">Кошик порожній</p>
                )}
                {isFormVisible && basket.length > 0 && (
                    <Form onSubmit={handleOrderSubmit} />
                )}
                {!isFormVisible && orderPlaced && (
                    <p className="basket-page__text-order">Ваше замовлення прийнято, дякуємо!</p>
                )}
                {basket.length > 0 && (
                    <div className="basket-page__card-block">
                        <h1 className="basket-page__title">Ваше замовлення</h1>
                        <div className="basket-page__card-wrapper">
                            <ProductsList
                                items={basket}
                                onBuyClick={() => {}}
                                showStar={true}
                                showDelete={true}
                                showButtonBuy={false}
                                isBasketPage={true}
                            />
                        </div>
                        <h1 className="basket-page__title-bottom">Сума замовлення {sumOrder} грн</h1>
                    </div>
                )}
            </div>
            {modal.isModalOpen && modal.modalType === "image" && (
                <ModalImage
                    data={modal.modalData}
                    bodyQuestion={<p className="modal-body__text">ВИДАЛИТИ ТОВАР З КОШИКА?</p>}
                    onConfirm={confirmDeletionHandler}
                    onClose={() => dispatch(closeModal())}
                />
            )}
        </section>
    );
}

export default BasketPage;

